
    var client = new ozpIwc.Client({peerUrl: "http://ozone-development.github.io/ozp-iwc"});

    client.connect().then(function(){
        document.getElementById('output').innerHTML = "Client connected with address: " + client.address;
    });
    client.data().set("/foo",{entity: "Hello world!"});
    client.data().get("/foo").then(function(a){
        document.getElementById("output2").innerHTML = a.entity;
    });

var data = client.data();
var balls = {};
var viewport = $("#viewport");

//===========================
// IWC Functionality
//===========================

// Set the pattern for the collection
data.set("/github/examples/balls", {
  pattern: "/github/examples/balls/"
});

// Generates a new ball on the canvas by gathering a ball from the IWC
var ballGen = function(name) {
  if (!balls[name]) {
    balls[name] = new Ball(name);
  }
};

// Watch for any changes to the collection
var onChange = function(reply) {
  console.log(reply);
  // itterate through the new collection and add any new ball
  reply.entity.newCollection.forEach(ballGen);
}
var initialCollection = function(resp){
  console.log("A",resp);
  resp.collection = resp.collection || [];
  resp.collection.forEach(ballGen);
};

data.watch("/github/examples/balls",{
  pattern: "/github/examples/balls/"
},onChange).then(initialCollection);

//===========================
// Ball Class & Drawing Functionality
//===========================
var Ball = function(name) {

  // IWC Shared values
  this.resource = name;

  // SVG Ball generation
  this.svg = viewport;
  this.el = document.createElementNS("http://www.w3.org/2000/svg", 'g');
  this.el.setAttribute("class", "ball");
  this.circle = document.createElementNS("http://www.w3.org/2000/svg", 'circle');
  this.el.appendChild(this.circle);
  this.label = document.createElementNS("http://www.w3.org/2000/svg", 'text');
  this.label.setAttribute("class", "svgHidden label");
    this.label.textContent=name;
  this.el.appendChild(this.label);

  this.svg.append(this.el);

  var self = this;

  // When the state of the ball changes, draw its updates
  var onBallChange = function(reply) {
    self.draw(reply.entity.newValue);
  };

  data.watch(this.resource, onBallChange).then(function(resp) {
    //keep reference of the watch registration to externally remove it
    self.watchReg = {
      msgId: resp.replyTo,
      src: resp.dst
    };
    self.draw(resp.entity);
  });

  $(this.el).click(function() {
        if(self.label.getAttribute("class").match("svgHidden")) {
            self.label.setAttribute("class","label");
        }else {
            self.label.setAttribute("class","svgHidden label");
        }
    });
};

// Updates the ball state and draw on the canvas
Ball.prototype.draw = function(state) {
  if (!state) {
    this.remove();
  }
  this.entity = {
    x: state.x || 0,
    y: state.y || 0,
    radius: state.radius || 5,
    color: state.color || "#111"
  };
  this.el.setAttribute("transform", "translate(" + state.x + "," + state.y + ")");
  this.circle.setAttribute("r", state.radius);
  this.circle.setAttribute("fill", state.color);
  this.label.setAttribute("x", state.radius + 2);

};

// Stops the ball from drawing to the canvas
Ball.prototype.remove = function() {
  // Stop receiving updates
  data.unwatch(this.resource, this.watchReg);

  // Clear it from the canvas
  this.el.setAttribute('display', 'none');

  // Remove its global reference
  balls[this.resource] = undefined;
};